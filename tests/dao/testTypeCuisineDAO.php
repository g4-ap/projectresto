<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title>RestoDAO : tests unitaires</title>
    </head>

    <body>

        <?php
        
        use modele\dao\TypeCuisineDAO;
        use modele\dao\Bdd;
        
        require_once '../../includes/autoload.inc.php';
        
        try {
            Bdd::connecter();
            ?>
            <h2>Test TypeCuisineDAO</h2>

            <h3>1- getOneById</h3>
            <?php $idR = 6; ?>
            <p>Le type n° <?= $idR ?></p>
            <?php
            $leType = TypeCuisineDAO::getOneById($idR);
            var_dump($leType);
            
            ?>
            <h3>2- getAll</h3>
            <p>Tous les types</p>
            <?php
            $lesTypes = TypeCuisineDAO::getAll();
            var_dump($lesTypes);
            
            ?>
            <h3>3- getAllByResto</h3>
            <p>Tous les types d'un Restaurant</p>
            <?php
            $lesTypes = TypeCuisineDAO::getAllByResto(4);
            var_dump($lesTypes);
            
            ?>

            <?php            
            Bdd::deconnecter();
        } catch (Exception $ex) {
            ?>
            <h4>*** Erreur récupérée : <br/> <?= $ex->getMessage() ?> <br/>***</h4>
            <?php
        }
        ?>

    </body>
</html>
