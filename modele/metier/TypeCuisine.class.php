<?php
namespace modele\metier;

class TypeCuisine {
    
    private int $idTC;
    private string $libelle;
    
    function __construct(int $idTC,string $libelle) {
        $this->idTC = $idTC;
        $this->libelle = $libelle;
    }
    
    function __toString() {
        return $this->libelle;
    }
    
    public function getIdTC(): int {
        return $this->idTC;
    }

    public function setIdTC(int $idTC): void {
        $this->idTC = $idTC;
    }
    
    public function getLibelle(): string {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): void {
        $this->libelle = $libelle;
    }


}

