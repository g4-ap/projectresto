<?php

namespace modele\dao;

use modele\metier\TypeCuisine;
use modele\dao\Bdd;
use PDO;
use PDOException;
use Exception;

class TypeCuisineDAO {
    
    public static function getOneById(int $id): ?TypeCuisine {
        $leType = null;
        try {
            $requete = "SELECT * FROM typesResto WHERE idTC = :idTC";
            $stmt = Bdd::getConnexion()->prepare($requete);
            $stmt->bindParam(':idTC', $id, PDO::PARAM_INT);
            $ok = $stmt->execute();
            // attention, $ok = true pour un select ne retournant aucune ligne
            if ($ok && $stmt->rowCount() > 0) {
                // Extraire l'enregistrement obtenu
                $enreg = $stmt->fetch(PDO::FETCH_ASSOC);
                //Instancier un nouveau restaurant
                $leType = new TypeCuisine($enreg['idTC'],$enreg['libelleTC']);
            }
        } catch (PDOException $e) {
            throw new Exception("Erreur dans la méthode " . get_called_class() . "::getOneById : <br/>" . $e->getMessage());
        }
        return $leType;
    }
    
        public static function getAll(): array {
            $lesTypes = array();
            try {
                $requete = "SELECT * FROM typesResto";
                $stmt = Bdd::getConnexion()->query($requete);
                while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $lesTypes[] = new TypeCuisine($enreg['idTC'],$enreg['libelleTC']); 
                }
            } catch (PDOException $e) {
                throw new Exception("Erreur dans la méthode " . get_called_class() . "::getAll : <br/>" . $e->getMessage());
            }
            return $lesTypes;
        }
        
        public static function getAllByResto(int $idR) : ?array {
        $lesTypes = array();
        try {
            $requete = "SELECT ty.idTC,ty.libelleTC FROM typesResto ty "
                    . " INNER JOIN liaisontypes li ON ty.idTC = li.idTypes"
                    . " WHERE li.idResto = :idR";
            $stmt = Bdd::getConnexion()->prepare($requete);
            $stmt->bindParam(':idR', $idR, PDO::PARAM_INT);
            $ok = $stmt->execute();
            if ($ok) {
                // Pour chaque enregistrement
                while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    //Instancier un nouveau type et l'ajouter à la liste
                    $lesTypes[] = new TypeCuisine($enreg['idTC'],$enreg['libelleTC']);
                }
                return $lesTypes;
            } else {
                ajouterMessage("Détail resto : le type n'a pas été trouvé");
                return null;
            }
            
        } catch (PDOException $ex) {
            echo $ex;
        }
        
    }
}
