/*Ajouter idU et l'incrémenter*/
ALTER TABLE resto2.utilisateur
	ADD idU int;
SET @rank = 0;
UPDATE resto2.utilisateur
	SET idU = @rank := @rank+1;
	
/*Ajouter idU dans les autres tables et les lier*/	
ALTER TABLE resto2.critiquer
	ADD idU int;
ALTER TABLE resto2.aimer
	ADD idU int;
UPDATE resto2.critiquer cr
	INNER JOIN resto2.utilisateur ut ON cr.mailU = ut.mailU
	SET cr.idU = ut.idU;
UPDATE resto2.aimer ai
	INNER JOIN resto2.utilisateur ut ON ai.mailU = ut.mailU
	SET ai.idU = ut.idU;
	
/*Retirer les contraintes liées à MailU de utilisateur*/	
ALTER TABLE resto2.critiquer
	DROP FOREIGN KEY critiquer_ibfk_2;
ALTER TABLE resto2.aimer
	DROP FOREIGN KEY aimer_ibfk_2;
	
/*Changer la structure des tables en accord avec idU de utilisateur en clé primaire*/
ALTER TABLE resto2.utilisateur
	DROP PRIMARY KEY,
	MODIFY idU int AUTO_INCREMENT PRIMARY KEY;
ALTER TABLE resto2.critiquer
	DROP PRIMARY KEY,
	ADD PRIMARY KEY (idR,idU),
	ADD CONSTRAINT critiquer_ibfk_3 FOREIGN KEY (idU) REFERENCES utilisateur (idU),
	DROP COLUMN critiquer.mailU;
ALTER TABLE resto2.aimer
	DROP PRIMARY KEY,
	ADD PRIMARY KEY (idR,idU),
	ADD CONSTRAINT aimer_ibfk_3 FOREIGN KEY (idU) REFERENCES utilisateur (idU),
	DROP COLUMN aimer.mailU;









