CREATE TABLE `resto2`.`typesResto` (
`idTC` bigint(20) NOT NULL ,
 `libelleTC` VARCHAR(20) NOT NULL , 
 PRIMARY KEY (`idTC`)) 
 ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
 

INSERT INTO typesResto VALUES(1, "sud ouest");
INSERT INTO typesResto VALUES(2, "japonaisie");
INSERT INTO typesResto VALUES(3, "orientale");
INSERT INTO typesResto VALUES(4, "fastfood ");
INSERT INTO typesResto VALUES(5, "vegetarienne");
INSERT INTO typesResto VALUES(6, "vegan");
INSERT INTO typesResto VALUES(7, "crepe");
INSERT INTO typesResto VALUES(8, "sandwich");
INSERT INTO typesResto VALUES(9, "tartes");
INSERT INTO typesResto VALUES(10, "viande");
INSERT INTO typesResto VALUES(11, "grillade");


CREATE TABLE `resto2`.`liaisontypes` (`idResto` bigint(20) NOT NULL , `idTypes` bigint(20) NOT NULL ) ENGINE = InnoDB; 


INSERT INTO resto2.liaisontypes VALUES
(1, 1),
(2,1),
(3,3),
(4,1),
(4,8),
(4,11),
(5,3),
(6,6),
(7,11),
(8,10),
(9,1),
(10,1), 
(11,10);

ALTER TABLE resto2.liaisontypes
ADD PRIMARY KEY (idResto, idTypes);

ALTER TABLE liaisontypes
  ADD CONSTRAINT liaisontypes_ibfk_1 FOREIGN KEY (idResto) REFERENCES resto (idR),
  ADD CONSTRAINT liaisontypes_ibfk_2 FOREIGN KEY (idTypes) REFERENCES typesresto (idTC);
