CREATE TABLE `resto2`.`typesResto` (
`idTC` INT NOT NULL ,
 `libelleTC` VARCHAR(20) NOT NULL , 
 PRIMARY KEY (`idTC`)) 
 ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
 

INSERT INTO typesResto VALUES(1, "sud ouest");
INSERT INTO typesResto VALUES(2, "japonaisie");
INSERT INTO typesResto VALUES(3, "orientale");
INSERT INTO typesResto VALUES(4, "fastfood ");
INSERT INTO typesResto VALUES(5, "vegetarienne");
INSERT INTO typesResto VALUES(6, "vegan");
INSERT INTO typesResto VALUES(7, "crepe");
INSERT INTO typesResto VALUES(8, "sandwich");
INSERT INTO typesResto VALUES(9, "tartes");
INSERT INTO typesResto VALUES(10, "viande");
INSERT INTO typesResto VALUES(11, "grillade");
